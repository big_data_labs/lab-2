import os

import redis

from src.cofing import get_settings


settings = get_settings()
redis_client = redis.Redis(
    host=settings.redis_dsn.host,
    port=settings.redis_dsn.port,
    username=settings.redis_dsn.username,
    password=settings.redis_dsn.password,
)