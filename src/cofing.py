import os
from functools import lru_cache
from pathlib import Path

from pydantic import BaseModel, Field, RedisDsn
from pydantic_settings import BaseSettings, SettingsConfigDict

ROOT_DIR = Path(__file__).parent.parent
DATA_DIR = ROOT_DIR / "data"
LOGS_DIR = ROOT_DIR / "logs"

os.makedirs(LOGS_DIR, exist_ok=True)


class KNNModelParams(BaseModel):
    n_neighbors: int = Field(10, title="Кол-во соседей")


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file='.env', env_nested_delimiter='__')

    knn_model: KNNModelParams
    redis_dsn: RedisDsn = Field('redis://redis:6379/0', title="Redis DSN")


@lru_cache(maxsize=1)
def get_settings() -> Settings:
    return Settings()
