import uuid

import pandas as pd
from fastapi import FastAPI, Request, HTTPException
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.utils import get_openapi
from loguru import logger
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import HTMLResponse, JSONResponse

from src import cofing
from src.api.models import PredictRequest, PredictResponse
from src.ml.KNNClassificator import KNNClassificator
from src.redis.methods import set_request_data, get_request_data

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

model = KNNClassificator(cofing.DATA_DIR / "diabetes.csv")


@app.get("/ping")
def read_root():
    return {'Ans': "pong"}


@app.post("/predict")
async def predict(request: Request, predict_request: PredictRequest) -> PredictResponse:
    request_id = request.state.request_id
    logger.info("Get new request id=%s" % str(request_id))

    df = pd.DataFrame({
        'Pregnancies': [predict_request.pregnancies],
        'Glucose': [predict_request.glucose],
        'BloodPressure': [predict_request.glucose],
        'SkinThickness': [predict_request.skin_thickness],
        'Insulin': [predict_request.insulin],
        'BMI': [predict_request.bmi],
        'DiabetesPedigreeFunction': [predict_request.diabetes_pedigree_function],
        'Age': [predict_request.age]
    })

    # Save request data in Redis
    set_request_data(request_id, df)

    logger.info("Start processing request %s" % str(request_id))
    result = model.predict(df)
    logger.info("Request %s completed with result %s" % (str(request_id), result))

    return PredictResponse(request_id=str(request_id), outcome=result[0])


@app.get("/requests/{request_id}")
async def find_request_data(request_id: str) -> JSONResponse:
    try:
        uuid_request_id = uuid.UUID(request_id)
    except Exception:
        raise HTTPException(status_code=402, detail="Wrong request id")
    request_data = get_request_data(uuid_request_id)
    if request_data is not None:
        return JSONResponse(content={'data': request_data})
    return JSONResponse(content={'detail': "No such request_id"})


@app.get("/docs", response_class=HTMLResponse)
async def custom_swagger_ui_html():
    return get_swagger_ui_html(openapi_url="/openapi.json", title="API docs")


@app.get("/openapi.json", include_in_schema=False)
async def get_open_api_endpoint():
    return JSONResponse(get_openapi(title="lab_2", version="0.1.0", routes=app.routes))
