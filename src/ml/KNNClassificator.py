import pandas as pd
from loguru import logger

from sklearn.neighbors import KNeighborsClassifier

from src import cofing
from src.cofing import get_settings


class KNNClassificator:
    def __init__(self, dataset_path: str) -> None:
        settings = get_settings()
        self.knn = KNeighborsClassifier(n_neighbors=settings.knn_model.n_neighbors)
        self.__dataset_path = dataset_path
        self.__fit()
        logger.info("Successfully prepare ML algorithm")

    def __fit(self) -> None:
        df = pd.read_csv(self.__dataset_path)
        target = df['Outcome']
        df.drop(columns=['Outcome'], inplace=True)
        target.head()
        self.knn.fit(df, target)

    def predict(self, df: pd.DataFrame) -> list[int]:
        return self.knn.predict(df)
