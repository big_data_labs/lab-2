import pytest


@pytest.mark.asyncio
async def test_predict_model(sample_df_input_data, knn_model):
    result = knn_model.predict(sample_df_input_data)
    assert result[0] == 0
