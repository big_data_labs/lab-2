FROM python:3.11.9-bullseye

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1

WORKDIR app/

RUN curl -sSL https://install.python-poetry.org | python3 -
ENV PATH="${PATH}:/root/.local/bin"

COPY pyproject.toml pyproject.toml
RUN poetry config virtualenvs.create false && poetry install --only main

COPY . .
